﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
    private Rigidbody2D myBody;
    private Vector3 moveDirection;

    
    public float moveSpeed = 10.0f;
    public float jumpSpeed = 20;
    public float jumpTime = 0;
    public bool canJump;

    // Use this for initialization
    void Start () {
        myBody = GetComponent<Rigidbody2D>();
        moveSpeed = 10.0f;
        jumpSpeed = 20;
        jumpTime = 0;
	}

	// Update is called once per frame
	void FixedUpdate () {
        float moveHorizontal = Input.GetAxis("Horizontal");

        //Store our horizontal and vertical movements into a Vector2
        Vector2 movement = new Vector2(moveHorizontal, 0);

        //And apply the force to the sprite
        myBody.velocity = movement * moveSpeed;

        //Here we'll handle jumping. This function applies upward momentum to our character while they are holding Jump for up to 1 second
        float isJumping = Input.GetAxis("Jump");
        if (isJumping > 0 && canJump && (jumpTime <= 0.2)) {
            myBody.AddForce(Vector2.up * jumpSpeed, ForceMode2D.Impulse);
            jumpTime += Time.deltaTime;
        }
        //This funciton accounts for if they release the Jump button early. In this case, they cannot reactivate jump and must wait until they hit the ground to jump again.
        if ((isJumping == 0) && (jumpTime > 0)) {
            canJump = false;
            myBody.gravityScale = 50;
        }

    }

    //Called when our player hits the floor
    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.tag == "Ground") {
            jumpTime = 0;
            canJump = true;
            myBody.gravityScale = 30;
        }
    }
}
